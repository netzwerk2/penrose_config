use std::time::{Duration, Instant};

use penrose::{
    core::{helpers::spawn_for_output_with_args, xconnection::XConn, Hook},
    draw::{widget::Text, DrawContext, TextStyle, Widget},
    WindowManager,
};

pub struct SeparatorWidget {
    text: Text,
}

impl SeparatorWidget {
    pub fn new(separator: &str, style: &TextStyle) -> Self {
        Self {
            text: Text::new(separator, style, false, false),
        }
    }
}

impl<X: XConn> Hook<X> for SeparatorWidget {}

impl Widget for SeparatorWidget {
    fn draw(
        &mut self,
        ctx: &mut dyn DrawContext,
        screen: usize,
        screen_has_focus: bool,
        w: f64,
        h: f64,
    ) -> penrose::draw::Result<()> {
        self.text.draw(ctx, screen, screen_has_focus, w, h)
    }

    fn current_extent(
        &mut self,
        ctx: &mut dyn DrawContext,
        h: f64,
    ) -> penrose::draw::Result<(f64, f64)> {
        self.text.current_extent(ctx, h)
    }

    fn require_draw(&self) -> bool {
        self.text.require_draw()
    }

    fn is_greedy(&self) -> bool {
        false
    }
}

pub struct CommandWidget {
    command: Vec<String>,
    interval: Duration,
    last_updated: Instant,
    text: Text,
    prefix: String,
    suffix: String,
}

impl CommandWidget {
    pub fn new(
        command: Vec<&str>,
        interval: Duration,
        style: &TextStyle,
        prefix: &str,
        suffix: &str,
    ) -> Self {
        Self {
            command: command.into_iter().map(String::from).collect(),
            interval,
            last_updated: Instant::now(),
            text: Text::new("", style, false, false),
            prefix: String::from(prefix),
            suffix: String::from(suffix),
        }
    }

    pub fn execute(&self) -> penrose::Result<Vec<String>> {
        Ok(spawn_for_output_with_args(
            self.command[0].as_str(),
            &self.command[1..]
                .iter()
                .map(|a| a.as_str())
                .collect::<Vec<&str>>(),
        )?
        .trim()
        .split('\n')
        .map(String::from)
        .collect())
    }
}

impl<X: XConn> Hook<X> for CommandWidget {
    fn startup(&mut self, _: &mut WindowManager<X>) -> penrose::Result<()> {
        self.text.set_text(format!(
            "{}{}{}",
            self.prefix,
            self.execute()?[0].as_str(),
            self.suffix
        ));

        Ok(())
    }

    // TODO: Figure out a way to actually execute a command every interval
    fn event_handled(&mut self, _: &mut WindowManager<X>) -> penrose::Result<()> {
        let now = Instant::now();

        if now - self.last_updated > self.interval {
            self.text.set_text(format!(
                "{}{}{}",
                self.prefix,
                self.execute()?[0].as_str(),
                self.suffix
            ));

            self.last_updated = now;
        }

        Ok(())
    }
}

impl Widget for CommandWidget {
    fn draw(
        &mut self,
        ctx: &mut dyn DrawContext,
        screen: usize,
        screen_has_focus: bool,
        w: f64,
        h: f64,
    ) -> penrose::draw::Result<()> {
        self.text.draw(ctx, screen, screen_has_focus, w, h)
    }

    fn current_extent(
        &mut self,
        ctx: &mut dyn DrawContext,
        h: f64,
    ) -> penrose::draw::Result<(f64, f64)> {
        self.text.current_extent(ctx, h)
    }

    fn require_draw(&self) -> bool {
        self.text.require_draw()
    }

    fn is_greedy(&self) -> bool {
        false
    }
}

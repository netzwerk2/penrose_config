use penrose::core::bindings::KeyEventHandler;
use penrose::core::State;
use penrose::extensions::util::update_monitors_via_xrandr;
use penrose::x::XConn;
use tracing::info;

use crate::{MONITOR_0, MONITOR_1, MONITOR_POSITION};

pub fn redetect_monitors<X: XConn>() -> Box<dyn KeyEventHandler<X>> {
    info!("Update monitor setup");
    Box::new(|_: &mut State<X>, _: &X| {
        update_monitors_via_xrandr(MONITOR_0, MONITOR_1, MONITOR_POSITION)
    })
}

use penrose::core::WindowManager;
use penrose::pure::RelativePosition;
use penrose_ui::Position;

pub mod actions;
pub mod hooks;
pub mod layout_transformers;
pub mod menus;
// pub mod widgets;

pub type Wm<X> = WindowManager<X>;

pub const MONITOR_0: &str = "DP-5";
pub const MONITOR_1: &str = "HDMI-0";
pub const MONITOR_POSITION: RelativePosition = RelativePosition::Left;

// The colors are hardcoded, because the gen_keybindings macro only takes &str, but the format macro returns String.
//                                CURRENT_LINE_COLOR, FOREGROUND_COLOR, BLUE_COLOR
pub const LAUNCHER_APP: &str = "dmenu_run -nb #282a2e -nf #c5c8c6 -sb #81a2be";
pub const TERMINAL_APP: &str = "alacritty";
pub const SCREENSHOT_APP: &str = "spectacle";

pub const BORDER_WIDTH: u32 = 2;
pub const BAR_POSITION: Position = Position::Bottom;
pub const BAR_HEIGHT: u32 = 20;
pub const BAR_PADDING: (u32, u32) = (4, 0);
pub const UBUNTU_MONO: &str = "Ubuntu Mono";
pub const FONT_SIZE: u8 = 10;

pub const SEARCH_ENGINE: &str = "duckduckgo";

// Tomorrow Night Theme
pub const BACKGROUND_COLOR: u32 = 0x1d1f21ff;
pub const CURRENT_LINE_COLOR: u32 = 0x282a2eff;
pub const SELECTION_COLOR: u32 = 0x373b41ff;
pub const FOREGROUND_COLOR: u32 = 0xc5c8c6ff;
pub const COMMENT_COLOR: u32 = 0x969896ff;
pub const RED_COLOR: u32 = 0xcc6666ff;
pub const ORANGE_COLOR: u32 = 0xde935fff;
pub const YELLOW_COLOR: u32 = 0xf0c674ff;
pub const GREEN_COLOR: u32 = 0xb5bd68ff;
pub const AQUA_COLOR: u32 = 0x8abeb7ff;
pub const BLUE_COLOR: u32 = 0x81a2beff;
pub const PURPLE_COLOR: u32 = 0xb294bbff;

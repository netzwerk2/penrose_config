use std::convert::TryInto;

use penrose::builtin::actions::key_handler;
use penrose::core::bindings::KeyEventHandler;
use penrose::extensions::util::dmenu::{DMenu, DMenuConfig, MenuMatch};
use penrose::util::{spawn, spawn_with_args};
use penrose::x::query::ClassName;
use penrose::x::{XConn, XConnExt};
use tracing::info;

use crate::{BLUE_COLOR, CURRENT_LINE_COLOR, SEARCH_ENGINE};

fn dmenu_config(prompt: &str) -> DMenuConfig {
    DMenuConfig {
        bg_color: CURRENT_LINE_COLOR.try_into().unwrap(),
        selected_color: BLUE_COLOR.try_into().unwrap(),
        custom_prompt: Some(prompt.to_string()),
        ignore_case: true,
        ..Default::default()
    }
}

// TODO: Add a Menu trait

pub struct ExitMenu;

impl ExitMenu {
    pub fn open<X: XConn>() -> Box<dyn KeyEventHandler<X>> {
        key_handler(move |state, _| {
            let lines = vec!["Log Out", "Suspend", "Shut Down", "Reboot"];

            // penrose detects monitors in the reverse order of xrandr
            let screen_index =
                state.client_set.current_screen().index() - state.client_set.screens().count();
            let menu = DMenu::new(&dmenu_config("Exit >>> "), screen_index);

            if let Ok(MenuMatch::Line(index, choice)) = menu.build_menu(lines) {
                info!("Matched {:?} on line {} for ExitMenu", choice, index);

                match choice.as_str() {
                    "Log Out" => std::process::exit(0),
                    "Suspend" => spawn_with_args("systemctl", &["suspend"]),
                    "Shut Down" => spawn("poweroff"),
                    "Reboot" => spawn_with_args("poweroff", &["--reboot"]),
                    _ => unreachable!(),
                }
            } else {
                Ok(())
            }
        })
    }
}

pub struct SearchMenu;

impl SearchMenu {
    pub fn open<X: XConn>() -> Box<dyn KeyEventHandler<X>> {
        key_handler(move |state, x: &X| {
            // penrose detects monitors in the reverse order of xrandr
            let screen_index =
                state.client_set.current_screen().index() - state.client_set.screens().count();
            let menu = DMenu::new(&dmenu_config("Search >>> "), screen_index);

            if let Ok(MenuMatch::UserInput(choice)) = menu.build_menu(Vec::<String>::new()) {
                info!("Matched {:?} as query for SearchMenu", choice);

                let url = format!("https://{}.com/?q={}", SEARCH_ENGINE, choice);
                if spawn_with_args("firefox", &["--new-tab", url.as_str()]).is_err() {
                    return Ok(());
                }

                let firefox_client = state
                    .client_set
                    .clients()
                    .find(|&&c| x.query(&ClassName("firefox"), c).is_ok())
                    .copied();

                if let Some(client) = firefox_client {
                    x.modify_and_refresh(state, |cs| {
                        cs.focus_client(&client);
                    })
                } else {
                    Ok(())
                }
            } else {
                Ok(())
            }
        })
    }
}

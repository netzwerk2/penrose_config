use penrose::core::hooks::{ManageHook, StateHook};
use penrose::core::State;
use penrose::util::spawn;
use penrose::x::query::ClassName;
use penrose::x::{XConn, XConnExt};
use penrose::Xid;
use tracing::info;

// TODO: Just use SpawnOnStartup
#[derive(Clone)]
pub struct StartupCommand {
    path: String,
}

impl StartupCommand {
    pub fn new(path: impl Into<String>) -> Self {
        Self { path: path.into() }
    }
}

impl<X: XConn> StateHook<X> for StartupCommand {
    fn call(&mut self, _: &mut State<X>, _: &X) -> penrose::Result<()> {
        info!("Spawn startup command: {}", self.path);
        spawn(&self.path)
    }
}

pub struct DefaultWorkspace {
    class_name: &'static str,
    workspace: String,
}

impl DefaultWorkspace {
    pub fn new(class_name: &'static str, workspace: impl Into<String>) -> Self {
        Self {
            class_name,
            workspace: workspace.into(),
        }
    }
}

impl<X: XConn> ManageHook<X> for DefaultWorkspace {
    fn call(&mut self, client: Xid, state: &mut State<X>, x: &X) -> penrose::Result<()> {
        x.modify_and_refresh(state, |cs| {
            match x.query(&ClassName(self.class_name), client) {
                Ok(matched) if matched => cs.move_client_to_tag(&client, &self.workspace),
                _ => {}
            }
        })
    }
}

// pub struct DefaultWorkspaceLayouts {
//     workspace_layouts: HashMap<usize, String>,
// }
//
// impl DefaultWorkspaceLayouts {
//     pub fn new(workspace_layouts: Vec<(usize, impl Into<String>)>) -> Box<Self> {
//         Box::new(Self {
//             workspace_layouts: workspace_layouts
//                 .into_iter()
//                 .map(|(w, l)| (w, l.into()))
//                 .collect(),
//         })
//     }
// }
//
// impl<X: XConn> Hook<X> for DefaultWorkspaceLayouts {
//     fn startup(&mut self, wm: &mut WindowManager<X>) -> penrose::Result<()> {
//         for (workspace_index, layout) in self.workspace_layouts.iter() {
//             if let Some(workspace) = wm.workspace_mut(&Selector::Index(*workspace_index)) {
//                 workspace.try_set_layout(layout).ok_or(penrose::Error)?;
//                 workspace.se
//                 info!(
//                     "Set default layout {} for workspace {}",
//                     layout,
//                     workspace.name()
//                 );
//             }
//         }
//
//         Ok(())
//     }
// }
//

use penrose::core::layout::{Layout, LayoutTransformer};
use penrose::pure::geometry::Rect;

// Basically copied from ReserveTop
#[derive(Debug, Clone)]
pub struct ReserveBottom {
    pub layout: Box<dyn Layout>,
    pub px: u32,
}

impl ReserveBottom {
    pub fn wrap(layout: Box<dyn Layout>, px: u32) -> Box<dyn Layout> {
        Box::new(Self { layout, px })
    }
}

impl LayoutTransformer for ReserveBottom {
    fn transformed_name(&self) -> String {
        self.layout.name()
    }

    fn inner_mut(&mut self) -> &mut Box<dyn Layout> {
        &mut self.layout
    }

    fn transform_initial(&self, mut r: Rect) -> Rect {
        if r.w == 0 || r.h == 0 {
            return r;
        }

        r.h -= self.px;

        r
    }
}

use std::collections::HashMap;

use penrose::builtin::actions::floating::{float_focused, sink_focused};
use penrose::builtin::actions::{modify_with, send_layout_message, spawn};
use penrose::builtin::layout::messages::{ExpandMain, IncMain, ShrinkMain};
use penrose::builtin::layout::transformers::Gaps;
use penrose::builtin::layout::MainAndStack;
use penrose::core::bindings::parse_keybindings_with_xmodmap;
use penrose::core::{Config, WindowManager};
use penrose::extensions::actions::toggle_fullscreen;
use penrose::extensions::hooks::manage::FloatingCentered;
use penrose::extensions::hooks::{add_ewmh_hooks, add_named_scratchpads, NamedScratchPad};
use penrose::util::spawn_for_output_with_args;
use penrose::x::query::ClassName;
use penrose::x11rb::RustConn;
use penrose::{map, stack, Color};
use penrose_ui::bar::widgets::{CurrentLayout, RefreshText, Widget, Workspaces};
use penrose_ui::{StatusBar, TextStyle};
use tracing::Level;
use tracing_subscriber::FmtSubscriber;

use penrose_config::actions::redetect_monitors;
use penrose_config::hooks::DefaultWorkspace;
use penrose_config::hooks::StartupCommand;
use penrose_config::layout_transformers::ReserveBottom;
use penrose_config::menus::{ExitMenu, SearchMenu};
use penrose_config::{
    BACKGROUND_COLOR, BAR_HEIGHT, BAR_PADDING, BAR_POSITION, BLUE_COLOR, BORDER_WIDTH, FONT_SIZE, FOREGROUND_COLOR, LAUNCHER_APP, RED_COLOR, SCREENSHOT_APP, SELECTION_COLOR, TERMINAL_APP, UBUNTU_MONO
};

// use penrose_config::hooks::StartupCommand;
// use penrose_config::hooks::{DefaultWorkspaceLayouts, StartupWorkspace};
// use penrose_config::menus::{ExitMenu, SearchMenu};
// use penrose_config::widgets::{CommandWidget, SeparatorWidget};
// use penrose_config::{
//     BACKGROUND_COLOR, BAR_HEIGHT, BAR_PADDING, BAR_POSITION, BLUE_COLOR, FONT_SIZE,
//     FOREGROUND_COLOR, RED_COLOR, SELECTION_COLOR, UBUNTU_MONO,
// };

// TODO: Application overview (like xmonad)
// TODO: Script to "deploy" config (i.e. building the project and copying relevant files to a certain directory -> allows relative paths

fn main() -> penrose::Result<()> {
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::DEBUG)
        .with_line_number(true)
        .finish();

    if let Err(e) = tracing::subscriber::set_global_default(subscriber) {
        panic!("Unable to set tracing subscriber: {}", e);
    }

    // let workspaces = vec!["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"];

    // General config
    // let mut config_builder = Config::default().builder();
    // config_builder
    //     .workspaces(workspaces.clone())
    //     TODO: Move floating windows
    // .floating_classes(vec!["Godot", "bevy", "Yad", "origin.exe", "Spotify"])
    // .focused_border(RED_COLOR)?
    // .unfocused_border(SELECTION_COLOR)?
    // .gap_px(3)
    // .border_px(3)
    // .bar_height(BAR_HEIGHT)
    // .top_bar(match BAR_POSITION {
    //     Position::Top => true,
    //     Position::Bottom => false,
    // })
    // .show_bar(true);

    // let follow_focus_conf = LayoutConf {
    //     floating: false,
    //     gapless: true,
    //     follow_focus: true,
    //     allow_wrapping: false,
    // };

    // // let style = TextStyle {
    // //     fg: FOREGROUND_COLOR.try_into()?,
    // //     bg: Some(BACKGROUND_COLOR.try_into()?),
    // //     padding: BAR_PADDING,
    // // };
    // // let highlight: Color = BLUE_COLOR.try_into()?;
    // // let empty_ws: Color = SELECTION_COLOR.try_into()?;

    /*let widgets: Vec<Box<dyn Widget<_>>> = vec![
        Box::new(Workspaces::new(style, highlight, empty_ws)),
        Box::new(CurrentLayout::new(style)),
        Box::new(RefreshText::new(style, || {
            spawn_for_output_with_args("date", &["+%a %d.%m.%G %T"])
                .unwrap_or_default()
                .trim()
                .to_string()
            //spawn_for_output("/home/pdobler/CLionProjects/penrose_config/scripts/get_time.sh")
            //    .unwrap_or_default()
            //    .trim()
            //    .to_string()
        })),
        // Box::new(SeparatorWidget::new("|", &style)),
        // Box::new(CommandWidget::new(
        //     vec!["/home/pdobler/CLionProjects/penrose_config/scripts/get_volume.sh"],
        //     Duration::from_millis(500),
        //     &style,
        //     " ",
        //     "%",
        // )),
        // Box::new(SeparatorWidget::new("|", &style)),
        // Box::new(CommandWidget::new(
        //     vec!["date", "+%a %d.%m.%G %T"],
        //     Duration::from_secs(1),
        //     &style,
        //     "",
        //     "",
        // )),
    ];*/

    // let status_bar = status_bar(
    //     BAR_HEIGHT,
    //     &style,
    //     style.fg,
    //     style.bg.unwrap(),
    //     BAR_POSITION,
    // ).unwrap();

    // let status_bar = StatusBar::try_new(
    //     BAR_POSITION,
    //     BAR_HEIGHT,
    //     style.bg.unwrap(),
    //     UBUNTU_MONO,
    //     FONT_SIZE,
    //     widgets,
    // )
    // .unwrap();

    // TODO: constants

    // Default number of clients in the main layout area
    let n_main = 1;

    // Default percentage of the screen to fill with the main area of the layout
    let ratio = 0.6;
    let ratio_percentage = 0.1;

    let outer_px = 3;
    let inner_px = 3;

    let layouts = stack!(
        MainAndStack::side(n_main, ratio, ratio_percentage),
        MainAndStack::bottom(n_main, ratio, ratio_percentage)
    )
    .map(|layout| ReserveBottom::wrap(Gaps::wrap(layout, outer_px, inner_px), BAR_HEIGHT));

    let mut config = add_ewmh_hooks(Config {
        default_layouts: layouts,
        border_width: BORDER_WIDTH,
        focused_border: Color::new_from_hex(RED_COLOR),
        normal_border: Color::new_from_hex(SELECTION_COLOR),
        tags: (1..=10).map(|i| i.to_string()).collect(),
        ..Default::default()
    });

    // TODO: Add more scratchpads (discord, element, gnome-clocks)
    let (scratch_pad, toggle_scratch_pad) = NamedScratchPad::new(
        "spotify",
        "spotify",
        ClassName("Spotify"),
        FloatingCentered::new(0.78, 0.85),
        true,
    );

    // let workspace_layouts = vec![(8, "[papr]")];

    // TODO: Figure out how to have a scratchpad startup application
    // let mut startup_applications: XcbHooks = vec![
    //     StartupCommand::new("/home/pdobler/CLionProjects/penrose_config/startup_setup.sh"),
    //     StartupCommand::new("/home/pdobler/CLionProjects/penrose_config/startup_updates.sh"),
    //     StartupCommand::new("/home/pdobler/CLionProjects/penrose_config/startup_daemons.sh"),
    //     StartupCommand::new("/home/pdobler/CLionProjects/penrose_config/startup_applications.sh"),
    // ];
    // let mut hooks: XcbHooks = vec![
    //     LayoutSymbolAsRootName::new(),
    //     ClientSpawnRules::new(
    //         default_workspaces
    //             .into_iter()
    //             .map(|(c, w)| SpawnRule::ClassName(c, w - 1))
    //             .collect(),
    //     ),
    //     DefaultWorkspaceLayouts::new(
    //         workspace_layouts
    //             .into_iter()
    //             .map(|(w, l)| (w - 1, l))
    //             .collect(),
    //     ),
    //     scratchpad.get_hook(),
    //     Box::new(status_bar),
    //     StartupWorkspace::new(0),
    // ];
    // hooks.append(&mut startup_applications);

    let startup_hooks = vec![
        StartupCommand::new("/home/pdobler/CLionProjects/penrose_config/startup_polybar.sh"),
        StartupCommand::new("/home/pdobler/CLionProjects/penrose_config/startup_setup.sh"),
        StartupCommand::new("/home/pdobler/CLionProjects/penrose_config/startup_updates.sh"),
        StartupCommand::new("/home/pdobler/CLionProjects/penrose_config/startup_daemons.sh"),
        StartupCommand::new("/home/pdobler/CLionProjects/penrose_config/startup_applications.sh"),
    ];

    for startup_hook in startup_hooks {
        config.compose_or_set_startup_hook(startup_hook);
    }

    let default_workspaces = [
        ("firefox", 2),
        ("org.gnome.clocks", 10),
        ("Element", 8),
        ("discord", 8),
        ("Signal", 8),
        ("thunderbird", 9),
    ]
    .into_iter()
    .map(|(class, workspace)| DefaultWorkspace::new(class, workspace.to_string()));

    for default_workspace in default_workspaces {
        config.compose_or_set_manage_hook(default_workspace);
    }

    // TODO: Menu to switch to workspace matching program name

    // TODO: Keybind to switch active monitor (like startup hook)
    // TODO: Keybind to move floating window on top
    // TODO: move and resize floating windows
    let mut key_bindings = map! {
        map_keys: |k: &str| k.to_string();

        // Client management
        "M-Up" => modify_with(|cs| cs.focus_up()),
        "M-Down" => modify_with(|cs| cs.focus_down()),
        "M-Right" => modify_with(|cs| cs.focus_up()),
        "M-Left" => modify_with(|cs| cs.focus_down()),
        "M-k" => modify_with(|cs| cs.focus_up()),
        "M-j" => modify_with(|cs| cs.focus_down()),
        "M-l" => modify_with(|cs| cs.focus_up()),
        "M-h" => modify_with(|cs| cs.focus_down()),
        "M-S-Up" => modify_with(|cs| cs.swap_up()),
        "M-S-Down" => modify_with(|cs| cs.swap_down()),
        "M-S-Right" => modify_with(|cs| cs.swap_up()),
        "M-S-Left" => modify_with(|cs| cs.swap_down()),
        "M-S-k" => modify_with(|cs| cs.swap_up()),
        "M-S-j" => modify_with(|cs| cs.swap_down()),
        "M-S-l" => modify_with(|cs| cs.swap_up()),
        "M-S-h" => modify_with(|cs| cs.swap_down()),
        "M-f" => toggle_fullscreen(),
        "M-S-space" => float_focused(),
        "M-C-S-space" => sink_focused(),
        "M-S-q" => modify_with(|cs| cs.kill_focused()),
        "M-BackSpace" => Box::new(toggle_scratch_pad),

        // TODO: Keybind to switch focussed monitor
        // Workspace Management
        "M-C-S-Right" => modify_with(|cs| cs.drag_workspace_forward()),
        "M-C-S-Left" => modify_with(|cs| cs.drag_workspace_forward()),
        "M-C-S-l" => modify_with(|cs| cs.drag_workspace_forward()),
        "M-C-S-h" => modify_with(|cs| cs.drag_workspace_forward()),

        // Layout Management
        "M-A-Up" => send_layout_message(|| IncMain(1)),
        "M-A-Down" => send_layout_message(|| IncMain(-1)),
        "M-A-Right" => send_layout_message(|| ExpandMain),
        "M-A-Left" => send_layout_message(|| ShrinkMain),
        "M-A-k" => send_layout_message(|| IncMain(1)),
        "M-A-j" => send_layout_message(|| IncMain(-1)),
        "M-A-l" => send_layout_message(|| ExpandMain),
        "M-A-h" => send_layout_message(|| ShrinkMain),
        "M-e" => modify_with(|cs| cs.next_layout()),
        "M-s" => modify_with(|cs| cs.previous_layout()),

        // Volume/Media control
        "XF86AudioRaiseVolume" => spawn("pactl set-sink-volume @DEFAULT_SINK@ +10%"),
        "XF86AudioLowerVolume" => spawn("pactl set-sink-volume @DEFAULT_SINK@ -10%"),
        "XF86AudioMute" => spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle"),
        // TODO: Fix this keybind
        "XF86AudioPlay" => spawn("playerctl --player=spotify,exaile play-pause"),
        "XF86AudioStop" => spawn("playerctl --player=spotify,exaile stop"),
        "XF86AudioPrev" => spawn("playerctl --player=spotify,exaile previous"),
        "XF86AudioNext" => spawn("playerctl --player=spotify,exaile next"),

        // Redetect monitors
        "M-S-r" => redetect_monitors(),

        // Lock screen and exit
        // TODO: Look for a different lock screen
        "M-S-x" => spawn("i3lock-fancy-multimonitor -n -p"),
        "M-S-e" => ExitMenu::open(),

        // Set Wallpaper
        // TODO: Fix this keybind
        "M-w" => spawn("feh --randomize --bg-scale ~/Wallpapers/AutoDownload/*"),

        // Program launch
        "M-d" => spawn(LAUNCHER_APP),
        "M-S-D" => SearchMenu::open(),
        // TODO: Use some rust library to calculate
        // "M-c" => run_external!(LAUNCHER_CALC);
        "M-Return" => spawn(TERMINAL_APP),
        // TODO: Fix this keybind
        "Print" => spawn(SCREENSHOT_APP),
    };

    for tag in '1'..='9' {
        key_bindings.extend([
            (
                format!("M-{}", &tag),
                modify_with(move |cs| cs.focus_tag(tag.to_string())),
            ),
            (
                format!("M-S-{}", &tag),
                modify_with(move |cs| cs.move_focused_to_tag(&tag.to_string())),
            ),
        ]);
    }
    key_bindings.extend([
        (
            String::from("M-0"),
            modify_with(move |cs| cs.focus_tag(String::from("10"))),
        ),
        (
            String::from("M-S-0"),
            modify_with(move |cs| cs.move_focused_to_tag(String::from("10"))),
        ),
    ]);

    let connection = RustConn::new()?;
    let parsed_key_bindings = parse_keybindings_with_xmodmap(key_bindings)?;

    let mut window_manager = add_named_scratchpads(
        WindowManager::new(config, parsed_key_bindings, HashMap::new(), connection)?,
        vec![scratch_pad],
    );
    //window_manager = status_bar.add_to(window_manager);
    window_manager.run()?;

    Ok(())
}

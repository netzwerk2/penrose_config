#!/bin/sh

/usr/lib/notification-daemon-1.0/notification-daemon
playerctld daemon
blueman-applet &
picom --config ~/.config/compton/compton.conf --daemon > ~/picom.log
